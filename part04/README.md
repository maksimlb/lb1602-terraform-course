### A.

1. Create two ec2 instances using `count`
   * Please do not use more than two instances as it may impact your colleagues
2. Add instance number to `Name` tag
3. Modify outputs to include all ips
4. Optionally achieve the same with `for_each` with two key-value pairs
5. Optionally achieve the same with `for_each` with a set with two values
6. Destroy the infrastructure


### B.

1. Make sure you have destroyed the infrastructure from the previous exercise
2. Restore `part04` to original version
3. Add a new `variable` of type `bool` with name `create_instance`
4. Based on the defined variable conditionally create the `aws_instance.my_first_instance`
   * Remember to update the `output` as well
5. Destroy the infrastructure


### C.

1. Make sure you have destroyed the infrastructure from the previous exercise
2. Create new workspace with name `test`
3. Make sure the `test` workspace is selected
4. Add the workspace name to the instance name
5. Add workspace name to outputs
6. Apply the infrastructure
7. Check the UI. You should see a new instance with a workspace in the name created in the list.
8. Switch back to default workspace
9. Apply the infrastructure
10. Check the UI. You should see a two instances created.
11. Switch to the `test` workspace and destroy the infrastructure.
12. Check the UI. You should still see the instance from the `default` workspace
13. Switch to the `default` workspace and destroy the infrastructure.
14. Check the UI. There should be no instances.


### D.

1. Make sure you have destroyed the infrastructure from the previous exercise
2. Run `terraform code` within this module
3. Try
   1. `var.course_user`
   2. `var.myvariable`
   3. `"My course user is ${var.course_user}"`
   4. `split("", var.course_user)`
   5. `split("", upper(var.course_user))`
4. Look at the collections functions
5. E.g. Have a look at the examples of `length`, `merge`
6. Try executing `aws_instance.my_first_instance.public_ip`. Does it work?
7. Apply the infrastructure
8. Open `terraform console`. Try executing `aws_instance.my_first_instance.public_ip` again. Why does it work now?


### E.

1. Make sure you have destroyed the infrastructure from the previous exercise
2. Define local `tags` value
   * `tags = { "Managed By": "Terraform" }`
3. Merge the local `tags` value with the `Name` value in instance tags, so that instance tags would have both the Name and "common tags" defined in the local tags
   * Hint: look at the Terraform `merge` function 


### F.

1. Make sure you have destroyed the infrastructure from the previous exercise
2. Apply the infrastructure
3. Apply the infrastructure and add `-replace` flag with ec2 instance resource
4. Verify the ec2 instance is recreated


### G.

1. Make sure you have destroyed the infrastructure from the previous exercise
2. Add a `local-exec` provisioner on the ec2 instance that would output server ip to the console
3. Apply the infrastructure and verify you see command executed locally
