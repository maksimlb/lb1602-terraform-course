### A.

Note, this is somewhat _artificial_ exercise.
We do not want to focus too much on the provider complexities and rather focus on Terraform part.
Thus we re-use the concepts we have already seen in AWS -- security groups and instances.
Typically you will not do the following in the real-life infrastructures.

We would like to create have a _static_ part of our infrastructure.
In this this _static_ part we would like to create rules that we will reuse in other modules.

Then we would like to create a _dynamic_ part.
In this _dynamic_ part we will read the remote state of our _static_ part and use its output.

1. Create a _static_ root module that would
   * Use remote state with backend "s3"
       * The same way we have done in previous exercises
       * Though name your key `key    = XXX_static_state`
   * Create security group resource
      * For this exercise it does not matter what rules are in the security group
      * Just remember to use your `course_user` variable in the name and description
   * Output the created security group `id` via `output`
  
2. Create a _dynamic_ root module that would
   * Read the remote state (see example below)
   * Create instance and use the security group from _static_ module
     * Remember `vpc_security_group_ids` attribute
     * Referencing the read state like `data.terraform_remote_state.static.outputs.xxx`

```
data "terraform_remote_state" "static" {
  backend = "s3"

  config = {
    bucket = "532137852714-course-state"
    key    = "xxx_static_state"
    region = "eu-west-1"
    profile = "course"
  }
}
```

3. Delete the infrastructure
   * First delete _dynamic_ part
   * Then delete _static_ part
