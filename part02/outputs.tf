output "public_ip" {
  value       = aws_instance.my_first_instance.public_ip
  description = "The public IP of the web server"
}

output "module_output" {
  value = module.my_module.myoutput
}
