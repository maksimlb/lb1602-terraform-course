### A.

* Analyze how the child module is invoked and data outputted to the root module
* `terraform init`
* `terraform apply`
* `terraform destroy`


### B.

* Create a new module locally `instance`
* Move instance creation to the new module
  * Pass instance type as an argument
  * Pass the course user variable as a variable
  * Follow the file naming convention -- create `main.tf`, `outputs.tf`, `variables.tf`
  * Output the public ip from the module and use it in the root module
* Call the created `instance` module from your root module
* Inspect the outcome and make sure all the resources are created as expected
* How does your state file look like?


### C.

* Destroy the created infrastructure
