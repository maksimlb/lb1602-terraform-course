### A.

1. Initialize the plugins

```
terraform init
```

2. Inspect the generated files

3. Apply the infrastructure

```
terraform apply
```

4. Use a moment to understand suggested _plan_

5. Try applying 

6. Inspect the created machine in the UI: EC2 -> Instances.


### B.

1. Apply the infrastructure again

```
terraform apply
```

2. What do you get?


### C.

1. Terminate the instance from the UI

2. Apply the infrastructure again

```
terraform apply
```

3. What do you get?

4. Agree with the changes


### D.

1. Change the name of hte instance

2. Apply the infrastructure again

```
terraform apply
```

3. What do you get?

4. Agree with the changes


### E. 

We will try to find resources and data for Terraform providers. Please look at the examples as well!

You have two options. And you are encouraged to try them both:

* Navigate through the provider documentation
* Simply googling your way through. Official Terraform providers are popular. And simply googling "terraform aws load balancer" will get you what you want. Or alternatively simply googling the resources name in your searches (if you know the resource name obviously), e.g. "aws_lb".

Find:

* Resource to create AWS EC2 instance.
  * What arguments can you provide?
  * Are any of the arguments mandatory?
  * What are arguments do we specify in our infrastructure?
  * What do examples show?
* Resource to create Azure virtual machine
* Data resources that reads created AWS security groups
* Try to find other resources / data for the AWS / Azure infrastructure you know


### F.

* Change instance type of your instance to "t2.nano". Inspect the changes that you get on `terraform apply` command. Then apply.
* Then try adding a new tag "Managed-By: Terraform". Inspect the changes that you get on `terraform apply` command. Then apply. Verify changes in the UI.
* Change the `ami` image to a hardcoded values, e.g. `ami-0551bf2a5bb48bc5c`. Inspect the output. Why is instance re-created? 
* Undo all the changes and apply the changes.


### G.

* Define a variable `ami_image` with default value `ami-0551bf2a5bb48bc5c`
* Instead of using ami id that we have read from the `data`, use the define variable `ami_image`
* Apply the infrastructure


### H.

* Output `course_user` variable in `output`
* Apply the infrastructure and see the changes in the output
* Look at the `terraform.tfstate`. Can you find outputs there?


### I.
* Look at the attributes reference of `aws_instance`
* Output public DNS name in an `output`
* Apply the infrastructure and see the changes in the output
* Look at the `terraform.tfstate`. Can you find outputs there?


### J.

In this exercise we will associate _security groups_ with our instances. Security groups simply control which ports or which ip addresses our instance can be accessed from / to.

* Find Terraform AWS security group resource
* Define a security group resource
  * Do not define `vpc_id` for simplicity
  * For cidr blocks use `["0.0.0.0/0"]` and `["::/0"]`
  * Define a name of your security group with your `course_user` variable as a prefix
  * Define a `Name` tag with your `course_user` variable as a prefix
* Use the created security group in your instance
  * See `vpc_security_group_ids` attribute
  * What kind of attribute should I use from the security group?
* Apply the infrastructure
* Inspect the changes in the UI


### K.

This exercise is somewhat artificial. But it demonstrates the `TF_LOG` functionality that becomes useful in certain situations.

* Change `ami` to a wrong value, e.g. "abc"
* Apply
* Set `export TF_LOG=DEBUG`
* Apply again. Inspect the output. Do you see the communication with the AWS?
* Unset the debug variable `unset TF_LOG`
* Change `ami` back


### L.

* Destroy existing infrastructure
* Add the following block

```
terraform {
  backend "s3" {
    bucket = "532137852714-course-state"
    key    = XXX_state
    region = "eu-west-1"
    profile = "course"
  }
}
```

* Use your course user as a prefix for the `key`, e.g. `alpha_state`
  * Note variables are not allowed
* Apply the infrastructure (you might need to run run `terraform init` again)
* Inspect the created state in AWS S3
* Destroy the infrastructure
* Inspected the state in AWS S3


### M.

* Run `apply` to make sure you have not done any local changes.
* Split the single `myinfrastructure.tf` into
  * `main.tf`
  * `variables.tf`
  * `outputs.tf`
  * `versions.tf`
  * Other files you might find relevant, e.g. `ec2.tf`
* Run `apply` again. You should not see any changes in the infrastructure.


### N.

* Destroy the infrastructure
* Make sure there are not resources left "under your user"
